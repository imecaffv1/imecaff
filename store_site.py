from abc import ABCMeta, abstractmethod
from data_extractor import IExtractor, FromPageExtractor
import json

# package name is not that indicative, probably need to change to something else.

class Detail(object):
    """
    serves as a container for the extractors results
    every detail that is using an extractor should inherit from this class, otherwise main would not call its
    set_detail_data function.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self._extractor = None
        self.extractor = self._get_extractor()(self.get_name())
        self._data = None

    @staticmethod
    def get_name():
        """
        used to set the attribute name in the affiliate object.
        @return: the name of the detail
        @rtype: str
        """
        raise NotImplementedError("you should implement yourself and return name for the attribute")

    @property
    def extractor(self):
        """
        the extractor that is used to get the data for the detail.
        @return: the extractor that is used to get the data for the detail.
        @rtype: IExtractor
        """
        return self._extractor

    @extractor.setter
    def extractor(self, value):
        """
        the extractor that is used to get the data for the detail. if it doesn't inherit from IExtractor, an exception
        will be raised
        @param value: the extractor the detail should use
        @type value: IExtractor
        """
        if not isinstance(value, IExtractor):
            raise TypeError("extractor must be of type Extractor, got {} instead".format(type(value)))
        else:
            self._extractor = value

    @property
    def data(self):
        """
        contains the results from the extractor. you can choose to implement the property in the detail object
        and doc the rtype there or look at the extractor documented function
        @return: the data from the extractor
        """
        return self._data

    @data.setter
    def data(self, value):
        """
        the data from the extractor
        @param value: data from the extractor
        """
        self._data = value

    def set_detail_data(self, url, parsed_site):
        """
        function called as part of factory design to call extractor "main" function, according to extractor type.
        can be refactored in future if Detail will have more than one extractor (or inherit and override)

        @param url: the url of the site as returned from the search engine
        @type url: str
        @param parsed_site: the relevant parsed site pages, see docstring for function in imecaff_utils.py
        @type parsed_site: dict
        """
        if isinstance(self.extractor, FromPageExtractor):
            self.data = self.extractor.extract_from_page(url, parsed_site)
        else:
            self.data = self.extractor.extract_from_page(url)

    @abstractmethod
    def _get_extractor(self):
        """
        returns the extractor assigned to that detail. each detail has to implement this with its relevant extractor
        @return: the extractor to use for that detail
        @rtype: IExtractor
        """
        raise NotImplementedError("You should implement this!")


class Affiliate(object):

    def __init__(self, domain):
        """
        applies domain argument to domain attribute, required for the process further in code
        @param domain: the domain object from the search engine
        @type domain: Domain
        """
        # PEP 8 requires members to be initialized within init
        self._domain = None
        self.domain = domain

    def __iter__(self):
        """
        implemented only for use when serializing to json for REST API calls.
        @return: iterator for all affiliate attributes
        @rtype: dict
        """
        for attr, value in iter(self.__dict__.items()):
            yield attr, value

    @property
    def name(self):
        """
        used in initial design, not really used now
        @rtype: str
        """
        return self._name

    @name.setter
    def name(self, value):
        """
        used in initial design, not really used now
        """
        self._name = value

    @property
    def domain(self):
        """
        the domain object for the affiliate, see docstring about Domain object
        @return: the domain object for the affiliate
        @rtype: Domain
        """
        return self._domain

    @domain.setter
    def domain(self, value):
        """
        if the value is not of type domain, will raise an exception
        @param value:
        @type value: Domain
        """
        if isinstance(value, Domain):
            self._domain = value
        else:
            raise TypeError("the domain member can only be of type domain")

class Domain(object):
    """
    holds initial data received from the search engine, which is needed for further enrichment of the affiliate object
    """
    def __init__(self, base_url, found_by_search_engine, rank):
        """

        @param base_url: the url as returned from the search engine
        @type base_url: str
        @param found_by_search_engine: the name of the search engine that was used, since rank might vary between
        different search engines.
        @type found_by_search_engine: str
        @param rank: the rank the url had inside the results from the search engine
        @type rank: int
        """
        self._base_url = base_url
        self._rank = rank
        self._found_by_search_engine = found_by_search_engine

    @property
    def data(self):
        """
        implemented to fit structure of other attributes of affiliate when trying to extract their data
        @return: string containing the __dict__ of Domain
        @rtype: str
        """
        return json.dumps(self.__dict__)

    @property
    def base_url(self):
        """

        @return: the url as returned from the search engine
        @rtype: str
        """
        return self._base_url

    @base_url.setter
    def base_url(self, value):
        """
        @param value: the url as returned from the search engine
        @type value: str
        """
        self._base_url = value

    @property
    def rank(self):
        """
        the rank the url had inside the results from the search engine
        @return: the rank the url had inside the results from the search engine
        @rtype: int
        """
        return self._rank

    @rank.setter
    def rank(self, value):
        """
        @param value: the rank the url had inside the results from the search engine
        @type value: int
        """
        self._rank = value

    @property
    def found_by_search_engine(self):
        """
        the name of the search engine that was used, since rank might vary between
        different search engines.
        @return: the name of the search engine that was used, since rank might vary between
        different search engines.
        @rtype: str
        """
        return self._found_by_search_engine

    @found_by_search_engine.setter
    def found_by_search_engine(self, value):
        """
        @param value: the name of the search engine that was used, since rank might vary between
        different search engines.
        @type value: str
        """
        self._found_by_search_engine = value
