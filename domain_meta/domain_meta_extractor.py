from data_extractor import IExtractor
from abc import ABCMeta, abstractmethod
import os
import json
import requests
import whois

class BaseDomainMetaExtractor(IExtractor):

    def __init__(self, dir_name):
        super(BaseDomainMetaExtractor, self).__init__(dir_name=dir_name)
        self._age = None
        self._page_info = None
        self._hosting = None
        self._owner = None
        self._endpoint_url = None
        self.config_data = self.get_params_from_config()

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        self._age = value

    @property
    def page_info(self):
        return self._page_info

    @page_info.setter
    def page_info(self, value):
        self._page_info = value

    @property
    def hosting(self):
        return self._hosting

    @hosting.setter
    def hosting(self, value):
        self._hosting = value

    @property
    def owner(self):
        return self._owner

    @owner.setter
    def owner(self, value):
        self._owner = value

    @property
    def endpoint_url(self):
        return self._endpoint_url

    @endpoint_url.setter
    def endpoint_url(self, value):
        self._endpoint_url = value

    @staticmethod
    def get_service_name():
        raise NotImplementedError("return name according to name of service used")

    def get_params_from_config(self):
        path_to_config = os.path.join(os.path.abspath(self.dir_name), 'config.json')
        with open(path_to_config) as json_file:
            config_data = json.load(json_file)
            return config_data.get(self.get_service_name())


class WhoisDomainMetaExtractor(BaseDomainMetaExtractor):

    @staticmethod
    def get_service_name():
        return "whois"

    def extract_from_page(self, url):
        return whois.whois(url)
