from store_site import Detail
from domain_meta.domain_meta_extractor import WhoisDomainMetaExtractor

class DomainMetaData(Detail):

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "domain_meta"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: WhoisDomainMetaExtractor
        """
        return WhoisDomainMetaExtractor