from store_site import Detail
from alexa_ranking.alexa_ranking_extractor import AlexaRankingExtractor

class AlexaRankingData(Detail):
    """
    stores the alexa ranking of the website, as received from alexa API
    """

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "alexa_ranking"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: AlexaRankingExtractor
        """
        return AlexaRankingExtractor