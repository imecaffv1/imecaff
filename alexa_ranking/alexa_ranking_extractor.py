from data_extractor import IExtractor
import requests
import xmltodict


class AlexaRankingExtractor(IExtractor):

    def extract_from_page(self, url):
        """
        @param url: the url of the main site, only here to match signature
        @type url: str
        @return: the alexa ranking of the site
        @rtype: str
        """

        # this alexa ranking url is not official nor documented, if suddenly stop working might need to move to
        # the paid API

        alexa_res_xml = requests.get('http://data.alexa.com/data?cli=10&dat=s&ver=quirk-searchstatus&url={}'.format(url))
        alexa_dict = xmltodict.parse(alexa_res_xml.text)
        try:
            return alexa_dict['ALEXA']['SD'][1]['POPULARITY']['@TEXT']
        except Exception as e:

            # TODO: add logging system for this error
            print(e)
            return "no alexa ranking or xml structure changed"

