from store_site import Detail
from about_us.about_us_extractor import AboutUsExtractor

class AboutUsData(Detail):
    """
    stores the data about the website, content of about us pages
    """

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "about_us"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: AboutUsExtractor
        """
        return AboutUsExtractor
