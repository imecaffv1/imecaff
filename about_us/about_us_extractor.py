from data_extractor import FromPageExtractor


class AboutUsExtractor(FromPageExtractor):

    def get_regex_from_keywords_file(self, dir_name):
        """
        here so would not look for file with keywords
        @param dir_name: the directory of the folder, just to match signature
        @type dir_name: str
        @return: no return
        @rtype: None
        """
        pass

    def extract_from_page(self, url, parsed_site):
        """
        looks for all text elements and returns their content, needs refining in future
        @param url: not really needed, just to match signature
        @type url: str
        @param parsed_site: all parsed pages of the site contains
        @type parsed_site: dict
        @return: a list of all strings found in text elements in the page
        @rtype: list
        """
        if parsed_site.get('about'):
            return [element.text for element in parsed_site.get('about')[0].find_all('p')]