from urllib.request import urlopen
from bs4.element import Tag
import re
from urllib.parse import urlparse
from data_extractor import FromPageExtractor

# regex that matches email addresses
email_extractor_regex = re.compile(r'([a-zA-Z0-9_.+\-]+[\s+@]+[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\w)', flags=2)



class EmailsExtractor(FromPageExtractor):
    """
    looks for email addresses in all texts and links inside all parsed pages of the site
    """

    def __init__(self, dir_name):
        """
        @param dir_name: the directory where the keywords file is
        @type dir_name: str
        """
        super(EmailsExtractor, self).__init__(dir_name=dir_name)
        self.version = "1"
        self._emails = []

    @property
    def emails(self):
        """
        the list of emails found by the extractor
        @return:  the list of emails found by the extractor
        @rtype: list
        """
        return self._emails

    @emails.setter
    def emails(self, value):
        """
        the list of emails found by the extractor
        @param value: the list of emails found by the extractor
        @type value: list
        """
        self._emails = value

    def get_email_from_tag(self, tag):
        """
        gets a Tag, and tries to extract an email if there one in the text or the link of tag.
        any email found is added to the list of the extractor.
        @param tag: the tag to search in
        @type tag: Tag
        """
        texts_to_search = [tag.get_text()]
        if tag.get('href') and tag.get('href').islower():
            texts_to_search += tag.get('href')

        # changed because now we search email common strings with spaces before and/or after @. Now we delete spaces in results
        self.emails += list([*map(lambda email: re.sub(r'\s', "", email), email_extractor_regex.findall(text))]
                            for text in texts_to_search)

    def extract_from_page(self, url, parsed_site):
        """
        going over all elements in all parsed pages of the site, trying to extract emails from them,
        returning a distinct list of emails found all across the website.
        @param url: here for signature, not used currently
        @type url: str
        @param parsed_site: the parsed site pages to look in
        @type parsed_site: dict
        @return: a distinct list of emails found all across the website.
        @rtype: list
        """
        for parsed_pages in iter(parsed_site.values()):
            for parsed_page in parsed_pages:
                try:
                    [self.get_email_from_tag(tag) for tag in parsed_page.find_all()]
                except Exception as e:
                    print(e)
        return list(set([item for sublist in self.emails for item in sublist]))




