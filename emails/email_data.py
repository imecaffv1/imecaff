from store_site import Detail
from emails.email_extractor import EmailsExtractor

class EmailData(Detail):

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "emails"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: EmailsExtractor
        """
        return EmailsExtractor

