from store_site import Detail
from banners.banners_extractor import BannersExtractor

class BannersData(Detail):
    """
    stores the data about all the banners that exist in the website.
    each banner object extracts all the data it can about the banner( merchant id, campaign id etc)
    """

    def __init__(self):
        super(BannersData, self).__init__()

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "banners"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: BannersExtractor
        """
        return BannersExtractor

