import sys
import os
from data_extractor import FromPageExtractor
import json
from collections import defaultdict
from selenium import webdriver
#import selenium.webdriver.chrome.service as service
import psutil
import time
from selenium.webdriver.support.ui import WebDriverWait
from urllib.parse import urlparse
import random
import bs4
from bs4.element import Tag
import re
from typing import Pattern
from abc import ABCMeta, abstractmethod
from selenium.webdriver.chrome.options import Options

banners_mapping = None

iframe_banners = False

class BannersExtractor(FromPageExtractor):

    def __init__(self, dir_name):
        """
        creates the banner extractor object and the banner mapping object to be used in the entire class
        @param dir_name: the directory where this file and the mapping lies
        @type dir_name: str
        """
        self._dir_name = dir_name

        global banners_mapping
        if not banners_mapping:
            path_to_mapping = os.path.join(os.path.abspath(self._dir_name), 'banners_mapping.json')
            with open(path_to_mapping, 'r') as keywords_file:
                banners_mapping = json.load(keywords_file)

        super(BannersExtractor, self).__init__(dir_name=dir_name)
        self._banners = []

    def get_regex_from_keywords_file(self, dir_name):
        """
        overriding the original func because the keywords file was already parsed, and structure is different

        @param dir_name: only here to match function signature
        @type dir_name: str
        @return: a regex that will match any known ad link according to our mapping
        @rtype: Pattern
        """
        return re.compile(r'|'.join(banners_mapping.keys()), flags=2)

    @property
    def banners(self):
        """
        a list that holds all the objects of the banners
        @return: list that holds all the objects of the banners
        @rtype: list
        """
        return self._banners

    @banners.setter
    def banners(self, value):
        """
        no reason to use this, just append to list. can be converted to "add" method so it will verify only banner
        object are added

        @param value:
        @type value: list
        @return:
        @rtype: None
        """
        self._banners = value

    def extract_from_page(self, url, parsed_site):
        """
        finds all links on main page which are matching one of the services we know,
        building a banner object for (if it has a specific one, if not the generic one) and returns
        the list

        @param url: the url of the site
        @type url: str
        @param parsed_site: dictionary holding parsed pages of site
        @type parsed_site: dict
        @return: list of the banner objects
        @rtype: list
        """

        # getting regular banners from links
        # use pycharm if needed to convert to regular for loops, but it is crucial that for runtime it stays as list
        # comprehension to reduce performance.

        try:
            self.banners += [banner_factory(self.regex.findall(i.get('href'))[0], i)
                       for i in [i for i in parsed_site.get('main_page')[0].find_all('a') if i.get('href') and i.get('href').islower()]
                       if self.regex.findall(i.get('href'))]
        except Exception as e:
            print(url, e)



        # getting special google banners from Iframes - can be removed if you want to speed up process and
        # willing to give up the google banners
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        prefs = {'profile.managed_default_content_settings.images': 2}
        chrome_options.add_experimental_option("prefs", prefs)

        global args
        global iframe_banners
        if iframe_banners:
            # this is important for context management, keep the selenium chromedriver usage inside context manager otherwise
            # you might end up with abandoned chrome processes with no one to close them.
            with webdriver.Chrome(chrome_options=chrome_options) as driver:
                driver.get(url)
                #failing = True
                cntr = 0
                while cntr < 100:

                    try:
                        # this is done inside a 'try catch' to ensure the iframes are stable and not disappearing on page.
                        # when the first list comprehension succeeds, it means the iframes are stable and we can strat extracting them.

                        iframes = driver.find_elements_by_tag_name("iframe")
                        potential_banners = [(i.get_attribute('id'), i.size) for i in iframes]
                        new_parsed_site = bs4.BeautifulSoup(driver.page_source, "lxml")
                        self.banners += [banner_factory(self.regex.findall(t.get('id'))[0], t)
                                                for t in [t for t in new_parsed_site.find_all('iframe') if t.get('id')]
                                                if self.regex.findall(t.get('id'))]
                        break
                    except Exception as e:
                        time.sleep(0.1)
                        cntr += 1
                        pass

                driver.quit()

        return self.banners





def MakeRegister():
    """
    $$$$$$$
    WARNING:

    this function is magic (seriously, pure magic) which I edited a bit to fit the needs of this design. if you
    are not sure what you are doing, don't touch it.

    $$$$$$$

    use this function to register banner type classes to the banner factory, by adding the @reg decorator on top of the
    class. make sure your class also implements the "name" function according to IBanner, and according to the value at
    banners_mapping.json

    """
    registry = {}
    def register(banner):

        registry[banner.name()] = banner
        return banner  # normally a decorator returns a wrapped function,
        # but here we return func unmodified, after registering it
    register.all = registry
    return register

reg = MakeRegister()

class IBanner(object):
    """
    the interface for the banner classes
    """
    __metaclass__ = ABCMeta

    @staticmethod
    @abstractmethod
    def name():
        pass


@reg
class Banner(IBanner):
    """
    the basic generic banner class, which will be used if there is no specific implementation for the type of banner
    """

    def __init__(self, url_match, service, element):
        """

        @param url_match: the string that matched the pattern in the link,
         should match the desired key in banner_mapping.json, taken care by the banner factory
        @type url_match: str
        @param service: the name of the service which provides the banner,
        should match the desired value in banner_mapping.json, taken care by the banner factory
        @type service: str
        @param element: the HTML element that holds the banner
        @type element: Tag
        """
        self._url = None
        self._url_match = url_match
        self._banner_service = service
        self._height = None
        self._width = None
        self._banner_id = None
        self._affiliate_id = None
        self._merchant_id = None
        self._offer_id = None
        self.set_banner_from_element(element)
        self.extract_specific_banner_data(element)
        self._alt_param = None
        self._network_name = None

    def extract_specific_banner_data(self, element):
        """
        extracts the extra data that is inside the element, if there is. each specific banner class should implement.
        implemented here so the child ones don't have to call it in their init..if you really want, can create another
        middleware class with this method as abstract and make banner classes as children.

        @param element: the HTML element that holds the banner extra data
        @type element: Tag
        @return: None
        @rtype: None
        """
        pass

    @staticmethod
    def name():
        """
        @rtype: str
        """
        return "generic"

    @property
    def banner_service(self):
        """
        @return: the name of the service which provides the banner
        @rtype: str
        """
        return self._banner_service

    @banner_service.setter
    def banner_service(self, value):
        """
        @param value: the name of the service which provides the banner
        @type value: str
        """
        self._banner_service = value

    @property
    def url(self):
        """
        @return: the url which matched a pattern of one of the banners known to us
        @rtype: str
        """
        return self._url

    @url.setter
    def url(self, value):
        """
        @param value: the url which matched a pattern of one of the banners known to us
        @type value: str
        """
        self._url = value

    @property
    def url_match(self):
        """
        @return: the string that matched the pattern in the link
        @rtype: str
        """
        return self._url_match

    @url_match.setter
    def url_match(self, value):
        """
        @param value: the string that matched the pattern in the link
        @type value: str
        """
        self._url_match = value

    @property
    def height(self):
        """
        @return: the height of the banner, used to represent banner size. will be None if no height in element
        @rtype: str or None
        """
        return self._height

    @height.setter
    def height(self, value):
        """
        @param value: the height of the banner, used to represent banner size. will be None if no height in element
        @type value: str or None
        """
        self._height = value

    @property
    def alt_param(self):
        """
        @return: the alt content of the element
        @rtype: str or None
        """
        return self._alt_param

    @alt_param.setter
    def alt_param(self, value):
        """
        @param value: the alt content of the element
        @type value: str or None
        """
        self._alt_param = value

    @property
    def width(self):
        """
        @return: the width of the banner, used to represent banner size. will be None if no width in element
        @rtype: str or None
        """
        return self._width

    @width.setter
    def width(self, value):
        """
        @param value: the width of the banner, used to represent banner size. will be None if no width in element
        @type value: str or None
        """
        self._width = value

    @property
    def banner_id(self):
        """
        @return: the id of the banner
        @rtype: str
        """
        return self._banner_id

    @banner_id.setter
    def banner_id(self, value):
        """

        @param value: the id of the banner
        @type value: str
        """
        self._banner_id = value

    @property
    def affiliate_id(self):
        """

        @return: the affiliate number for the banner
        @rtype: str
        """
        return self._affiliate_id

    @affiliate_id.setter
    def affiliate_id(self, value):
        """

        @param value: the affiliate number for the banner
        @type value: str
        """
        self._affiliate_id = value

    @property
    def network_name(self):
        """

        @return: network name for the banner
        @rtype: str
        """
        return self._network_name

    @network_name.setter
    def network_name(self, value):
        """

        @param value: network name for the banner
        @type value: str
        """
        self._network_name = value

    @property
    def merchant_id(self):
        """

        @return: the merchant id of the banner
        @rtype: str
        """
        return self._merchant_id

    @merchant_id.setter
    def merchant_id(self, value):
        """

        @param value: the merchant id of the banner
        @type value: str
        """
        self._merchant_id = value

    @property
    def offer_id(self):
        """
        @return: the offer id of the banner
        @rtype: str
        """
        return self._offer_id

    @offer_id.setter
    def offer_id(self, value):
        """
        @param value: the offer id of the banner
        @type value: str
        """
        self._offer_id = value

    @property
    def parsed_params(self):
        return urlparse(self.url).query

    @property
    def parsed_link(self):
        return urlparse(self.url).netloc

    def set_banner_from_element(self, element):
        """
        basic extraction of banner data,
        should be relevant to banners who don't have size attributes as well (just link)

        @param element: the element which holds the banner
        @type element: Tag
        """
        #
        if element.name == 'a':
            self.url = element['href']
            if element.img:
                self.height = element.img.get('height')
                self.width = element.img.get('width')
                self.alt = element.img.get('alt')

@reg
class GoogleIframeBanner(Banner):
    """
    the banner for google Iframe ads, not doing much now but this is a placeholder for future needs.
    """

    def __init__(self, url_match, service, element):
        super().__init__(url_match, service, element)

    @staticmethod
    def name():
        """
        @return: the name of the service this class belongs to, required for banner factory
        @rtype: str
        """
        return "google_iframe"

    def set_banner_from_element(self, element):
        """
        different from base because trying to extract size from iframe not from the image element.

        @param element: the iframe that holds the banner
        @type element: Tag
        """

        self.height = element.get('height')
        self.width = element.get('width')

@reg
class GoogleAdService(Banner):

    @staticmethod
    def name():
        return 'google_ad'

    def extract_specific_banner_data(self, element):
        pass

@reg
class GoogleAds(Banner):

    @staticmethod
    def name():
        return 'google_ads'

    def extract_specific_banner_data(self, element):
        pass

@reg
class ShareasaleBanner(Banner):
    """
    the shareasale service banner class. notice its 'share a sale' and not just 'share sale'.

    currently no other banner classes, but if more will have the affiliate number and merchant id members,
    create a middleware class which has these attributes and properties and inherit from it.
    """

    @staticmethod
    def name():
        """
        @return: the name of the service this class belongs to, required for banner factory
        @rtype: str
        """
        return "shareasale"


    def extract_specific_banner_data(self, element):
        """
        trying to extract the details hidden in the shareasale link, and apply the to the relevant attributes

        @param element: the element that holds the banner
        @type element: Tag
        """
        params_dict = {}
        [params_dict.update({param[0]: param[1]}) for param in [param.split("=") for param in self.parsed_params.split("&")]]
        self.banner_id = params_dict['b']
        self.affiliate_id = params_dict['u']
        self.merchant_id = params_dict['m']

@reg
class Aliexpress(Banner):

    @staticmethod
    def name():
        return "aliexpress_cdn"

    def extract_specific_banner_data(self, element):
        pass

@reg
class Hasoffers(Banner):

    @staticmethod
    def name():
        return "hasoffers"

    def extract_specific_banner_data(self, element):
        params_dict = {}
        [params_dict.update({param[0]: param[1]}) for param in [param.split("=") for param in self.parsed_params.split("&")]]
        self.affiliate_id = params_dict['aff_id']
        self.offer_id = params_dict['offer_id']
        self.network_name = self.parsed_link.split(".go2cloud.org", 1)[0]
@reg
class ClickbankBanner(Banner):

    @staticmethod
    def name():
        return "clickbank"

    def extract_specific_banner_data(self, element):
        self.affiliate_id = urlparse(self.url).netloc.split('.')[0]

@reg
class CjBanner(Banner):

    @staticmethod
    def name():
        return "cj"

    def extract_specific_banner_data(self, element):
        params = urlparse(self.url).path.split("-")
        self.affiliate_id = params[1]
        self.offer_id = params[2]

@reg
class VipResponse(Banner):

    @staticmethod
    def name():
        return "vip_response"

    def extract_specific_banner_data(self, element):
        params = urlparse(self.url).path.split('/')
        self.affiliate_id = params[1]
        self.offer_id = params[2]
@reg
class Rakuten(Banner):

    @staticmethod
    def name():
        return "rakuten"

    def extract_specific_banner_data(self, element):
        params_dict = {}
        [params_dict.update({param[0]: param[1]}) for param in [param.split("=") for param in self.parsed_params.split("&")]]
        self.offer_id = params_dict['offerid']
@reg
class Peerfly(Banner):

    @staticmethod
    def name():
        return "peerfly"

    def extract_specific_banner_data(self, element):
        params = urlparse(self.url).path.split('/')
        self.affiliate_id = params[1]
        self.offer_id = params[2]
@reg
class MaxBounty(Banner):

    @staticmethod
    def name():
        return "mb102"

    def extract_specific_banner_data(self, element):
        params_dict = {}
        [params_dict.update({param[0]: param[1]}) for param in [param.split("=") for param in self.parsed_params.split("&")]]
        self.banner_id = params_dict['c']
        self.affiliate_id = params_dict['a']
        self.offer_id = params_dict['o']


def banner_factory(match_url, element):
    """
    gets a url match on a link and the element it was in, and tries to find the right class for that kind of banner,
    in the dictionary of registered classes (all banner classes with @reg decorator). if not found, using the generic
    banner object.

    after finding the class,
    calls it's constructor with the match_url, element and the service which the banner belongs to

    @param match_url: the string that matched the pattern in the link,
    should match the desired key in banner_mapping.json . assuming the regex pattern for finding a banner is using the
    same mapping file and object.

    @type match_url: str
    @param element: the element which holds the banner
    @type element: Tag
    @return: the relevant banner instance, according to the params.
    @rtype: IBanner
    """
    banner_type = reg.all['generic']
    for banner_regex in banners_mapping.keys():
        if re.search(r'{}'.format(banner_regex), match_url, flags=2):
            service = banners_mapping[banner_regex]
            break

    # must have service in banner mapping otherwise would not be in regex, but might not have a specific banner object

    if reg.all.get(service):
        banner_type = reg.all.get(service)

    return banner_type(match_url, service,  element)










class BannerFactory(object):
    """
    was used in prev versions, not deleted for now
    """


    banner_factory = {"generic": Banner, "google_iframe": GoogleIframeBanner}

    @staticmethod
    def get_banner(match_url, element):
        """

        @param match_url:
        @type match_url:
        @param element:
        @type element:
        @return:
        @rtype: Banner
        """
        banner_class = Banner
        global banners_mapping
        matches_to_service = {tuple(v): k for k, v in banners_mapping.items()}
        for matches, service in iter(matches_to_service.items()):
            if match_url == matches:
                if BannerFactory.banner_factory.get(service):
                    banner_class = BannerFactory.banner_factory.get(service)
                return banner_class(match_url, service, element)

        raise KeyError("the banner is not of any vendor we know, should not have returned from regex")





