from data_extractor import FromPageExtractor


class MailingExtractor(FromPageExtractor):


    def extract_from_page(self, url, parsed_site):
        """
        using regex from all keywords for mailing list, and returns whether there is a mailing list or not.

        similar to cart extractor, separate classes for future use

        @param url: not really needed, just to match signature
        @type url: str
        @param parsed_site: the parsed site, dictionary where values are the parsed website pages
        @type parsed_site: dict
        @return: whether there is a mailing list on site or not
        @rtype: bool
        """

        for element in parsed_site.get('main_page')[0].find_all():
            if self.regex.findall(element.get_text()):
                return True
        return False
