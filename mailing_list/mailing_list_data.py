from store_site import Detail
from mailing_list.mailing_list_extractor import MailingExtractor

class MailingListData(Detail):

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "mailing_list"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: MailingExtractor
        """
        return MailingExtractor
