import argparse
from search_engines.search_engines_consts import Consts
from social_media.social_media_data import SocialMediaData
from emails.email_data import EmailData
from mailing_list.mailing_list_data import MailingListData
from banners.banners_data import BannersData
from keywords_enrichment.keywords_enrichment import KeywordEnricher
from alexa_ranking.alexa_ranking_data import AlexaRankingData
from cart.cart_data import CartData
from url_caching.url_caching import UrlCaching
from about_us.about_us_data import AboutUsData
import json
from store_site import Affiliate, IExtractor, Detail, Domain
from multiprocessing import Pool
from utils.imecaff_utils import page_parsing, get_parsed_pages
from banners import banners_extractor
import cProfile
import asyncio
import aiohttp
import os
import pickle
import base64

def get_affiliate(site, args):
    """

    gets one Domain object, creates the affiliate from it, calling all of it's extractors and returning the affiliate
    @param site: the domain object which contains the information for the affiliate
    @type site: Domain
    @param args: the arguments from the CLI/ function call imitating CLI
    @type args: Namespace
    @return: the affiliate object with all the data according to extractors
    @rtype: Affiliate
    """

    affiliate = Affiliate(site)
    print("starting {}".format(site.base_url))
    # done here to for performance issues, instead of each time for every extractor
    for detail in args.details:
        setattr(affiliate, '{}'.format(detail.get_name()), detail())
    full_parsed_site = get_parsed_pages(affiliate.domain.base_url)
    # try:
    #     full_parsed_site = get_parsed_pages(affiliate.domain.base_url)
    # except Exception as e:
    #     print(e)
    #     print("could not parse {}".format(affiliate.domain.base_url))
    #     return affiliate

    for attr, detail in affiliate:
        if isinstance(detail, Detail):
            detail.set_detail_data(affiliate.domain.base_url, full_parsed_site)
    #print("finished {}".format(affiliate.domain.base_url))
    return affiliate


args = None

def main(cmd_args=None):



    """

    Args:
        cmd_args ():

    Returns:

    """
    parser = argparse.ArgumentParser(description='Example with long option names')
    parser.add_argument('--mp', help="whether to use multiprocessing", action="store_true", dest="multiprocessing")
    parser.add_argument('--uc', help="whether to use url_caching", action="store_true", dest="url_caching")
    parser.add_argument('--se', help="which search engine to use", action="store", dest="search_engine", default="bing")
    parser.add_argument('--mc', help="which market code to use"
                                     ", see possibilities at https://docs.microsoft.com/en-us/rest/api/cognitiveservices/bing-custom-search-api-v7-reference#market-codes",
                        action="store", dest="market_code", default="en-US")

    parser.add_argument('--ct', help="which type of customer it is, for enrichment", action="store",
                        dest="customer_type", default="affiliate")
    parser.add_argument('--k', help="which keywords to search", action="store", dest="keywords")
    parser.add_argument('--ek', help="the list of keywords to exclude from the search, so that the sites returned "
                        "will not contain these words", action="store", dest="keywords_to_exclude",
                        default=[], nargs='+')
    parser.add_argument('--sm', help="whether to extract social media links", action="append_const",
                        const=SocialMediaData, dest="details")
    parser.add_argument('--em', help="whether to extract contacts emails", action="append_const",
                        const=EmailData, dest="details")
    parser.add_argument('--ml', help="whether to extract if there's a mailing list", action="append_const",
                        const=MailingListData, dest="details")
    parser.add_argument('--ba', help="whether to extract the banners from the site", action="append_const",
                        const=BannersData, dest="details")
    parser.add_argument('--iframe_ba', help="whether to extract the banners that require fetching iframes,"
                                            "such as google and some of amazon. this also means the runtime will be "
                                            "significantly extended", action="store_true",
                        dest="iframe_banners")
    parser.add_argument('--al', help="whether to extract the alexa ranking of the site", action="append_const",
                        const=AlexaRankingData, dest="details")
    parser.add_argument('--ca', help="whether to extract if there's a cart", action="append_const",
                        const=CartData, dest="details")
    parser.add_argument('--au', help="whether to extract the about us info from the site", action="append_const",
                        const=AboutUsData, dest="details")
    parser.add_argument('--nr', help="how many search results need to parse", type=int, action="store",
                        default=100, dest="number_of_results")
    # parser.add_argument('--dm', help="whether to extract the domain_metadata", action="append_const",
    #                     const=DomainMetaData, dest="details")
    items_to_return = {}
    affiliates = []
    affiliates_json = {}
    items_to_return['affiliates_json'] = affiliates_json
    items_to_return['failed_sites'] = {}
    args = parser.parse_args(cmd_args)
    args_details = []
    # arguments to parse
    for detail in args.details:
        args_details.append(detail.get_name())

    items_to_return['args_details'] = args_details

    if args.iframe_banners:
        banners_extractor.iframe_banners = args.iframe_banners

    # enriching keywords according to params
    enriched_keywords = KeywordEnricher.enrich_keyword(args.keywords, args.customer_type, args.keywords_to_exclude)

    # finding the correct search engine according to params
    search_engine = Consts.search_engines[args.search_engine]()

    sites = search_engine.find_sites_and_ranking(enriched_keywords, args.market_code, args.number_of_results)
    url_cacher = UrlCaching(set([domain.base_url for domain in sites]))

    if args.url_caching:
        items_to_return['cached_urls'] = list(url_cacher.already_extracted_urls)
        sites = [site for site in sites if site.base_url in url_cacher.urls_to_extract]

    API_USE = True

    if API_USE:

        # load api's links from json file
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        with open(BASE_DIR + '/imecaff_logic/api_links.json') as file:
            api_links = json.loads(file.read(), encoding='utf8')

        def split_sites(a, n):
            k, m = divmod(len(a), n)
            return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

        splitted_sites_list = list(split_sites(sites, 3))

        # params for call_url function in byte type
        # splited by hand (because there is no instances links right now)
        input_params = {
            "args": base64.b64encode(pickle.dumps(args)).decode('utf8'),
            "sites_1": base64.b64encode(pickle.dumps(splitted_sites_list[0])).decode('utf8'),
            "sites_2": base64.b64encode(pickle.dumps(splitted_sites_list[1])).decode('utf8'),
            "sites_3": base64.b64encode(pickle.dumps(splitted_sites_list[2])).decode('utf8')
        }

        # making an async request to our "API" servers
        async def call_url(url, params):

            print('Starting {}'.format(url))
            session = aiohttp.ClientSession()

            async with session.request("POST", url=url, data=params) as response:
                data = await response.text()
            print('Ended %s with result: %s'%(url, json.dumps(data)))
            return {url: data}

        futures = [call_url(url, input_params) for url in api_links['links']]

        # create a new event loop or there will be a conflict with django
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        # run our async processes
        result = loop.run_until_complete(asyncio.wait(futures))
        loop.close()

        for res in result:
            for r in res:
                try:
                    for key, value in r._result.items():
                        # output[key] = pickle.loads(base64.b64decode(value))
                        encoded_data = pickle.loads(base64.b64decode(value))
                        affiliates.extend(encoded_data['affiliates'])
                        for k, v in encoded_data['failed_sites'].items():
                            items_to_return['failed_sites'].update({k: v})

                except:
                    print("There is no response from the API")

    else:

        if args.multiprocessing:
            pool = Pool(50)
            temp = [(pool.apply_async(get_affiliate, (site, args,), callback=affiliates.append), site.base_url) for site in sites]
            for res in temp:
                try:
                    res[0].get(timeout=10)
                except Exception as e:
                    items_to_return['failed_sites'].update({res[1]: e})

        else:
            for site in sites:
                affiliates.append(get_affiliate(site, args))

    # cacher does not care about the exception for now, only that it failed
    url_cacher.failed_urls = [url for url in items_to_return['failed_sites'].keys()]
    #TODO: create serialize class and use it
    items_to_return['affiliates'] = affiliates
    for affiliate in affiliates:
        kaki = {}
        for k, v in iter(affiliate.__dict__.items()):
            kaki[k] = v.data
        affiliates_json[affiliate.domain.base_url] = json.dumps(kaki, ensure_ascii=False, default=str)

    print(items_to_return)
    return items_to_return







if __name__ == "__main__":

    # used for standalone development and debugging of the Back End of Imecaff
    cProfile.run('main()')