from store_site import Detail
from social_media.social_media_extractor import SocialMediaExtractor


class SocialMediaData(Detail):

    def __init__(self):
        super(SocialMediaData, self).__init__()

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: SocialMediaExtractor
        """
        return SocialMediaExtractor

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "social_media"
