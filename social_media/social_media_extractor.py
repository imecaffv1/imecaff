import requests
from html_to_etree import parse_html_bytes
from extract_social_media import find_links_tree
from data_extractor import FromPageExtractor


class SocialMediaExtractor(FromPageExtractor):

    def __init__(self, dir_name):

        super(SocialMediaExtractor, self).__init__(dir_name=dir_name)
        self.version = "1"

    def get_regex_from_keywords_file(self, dir_name):
        """
        regex not needed here, just to match signature.
        @param dir_name:
        @type dir_name:
        @return:
        @rtype:
        """
        pass

    def extract_from_page(self, url, parsed_site):
        return SocialMediaExtractor.extract_social_media_from_url(url)

    @staticmethod
    def extract_social_media_from_url(url):
        """
        requires another call instead of using parsed site, but it works that way so no need to touch.
        this is code from a ready library, look it up for reference.
        @param url: the url of the site to find the social media links at
        @type url: str
        @return: a list containing all the social media links on site
        @rtype: list
        """
        try:
            res = requests.get(url)
            tree = parse_html_bytes(res.content, res.headers.get('content-type'))
            return list(set(find_links_tree(tree)))
        except Exception as e:
            print("the site blocked direct url access {}".format(url))

            return []
