import os
import io
import json
import atexit

PATH_TO_CACHED_URLS_FILE = os.path.join(os.path.abspath("url_caching"), 'cached_urls.json')


class UrlCaching(object):
    """
    mechanism to reduce the amount of sites to exhaust, if we already did it in the past.
    """

    def __init__(self, urls_from_search_engine):
        """

        @param urls_from_search_engine: urls that were returned from the search engine
        @type urls_from_search_engine: set
        """
        self._cached_urls = None
        self._urls_from_search_engine = urls_from_search_engine
        self._cached_urls_dict = None
        self._failed_urls = None

        # read about atexit if needed. TL;DR: runs the function at the successful end of the python execution, so if we
        # failed for some reason we do not store the urls as 'cached urls'. the reason is that if we failed, it means
        # we never stored the data for these urls, and we want to avoid a case where we pass on a url as 'cached', but
        # it would not be found in DB.
        atexit.register(self.cache_new_urls)

    @property
    def failed_urls(self):
        """
        @return: the list of urls that were not extracted properly and no affiliate was returned for them
        @rtype: list
        """
        return self._failed_urls

    @failed_urls.setter
    def failed_urls(self, value):
        """
        @param value: the list of urls that were not extracted properly and no affiliate was returned for them
        @type value: list
        """
        self._failed_urls = value

    @property
    def cached_urls_dict(self):
        """
        the dictionary containing the urls that were already extracted
        @return: the dictionary containing the urls that were already extracted
        @rtype: dict
        """
        return self._cached_urls_dict

    @cached_urls_dict.setter
    def cached_urls_dict(self, value):
        """
        the dictionary containing the urls that were already extracted
        @param value: the dictionary containing the urls that were already extracted
        @type value: dict
        """
        self._cached_urls_dict = value

    @property
    def urls_from_search_engine(self):
        """
        @return: urls that were returned from the search engine
        @rtype: set
        """
        return self._urls_from_search_engine

    @urls_from_search_engine.setter
    def urls_from_search_engine(self, value):
        """
        @param value: urls that were returned from the search engine
        @type value: set
        """
        self._urls_from_search_engine = value

    @property
    def urls_to_extract(self):
        """
        @return: the list of urls that need to be extracted, since they do not appear in cached urls file
        @rtype: set
        """

        return self.urls_from_search_engine.difference(self.cached_urls)

    @property
    def already_extracted_urls(self):
        """
        the list of urls that appeared in the search engine response, but were already extracted before. in future could
        be the first stage of filtering (versions of extractors are updated etc)
        @return: list of urls that appeared in the search engine response, but were already extracted before
        @rtype: set
        """
        return self.urls_from_search_engine.intersection(self.cached_urls)

    def _get_cached_urls(self):
        """
        @return: returns the distinct list of the urls that were extracted before
        @rtype: set
        """
        with io.open(PATH_TO_CACHED_URLS_FILE, encoding='utf-8') as cached_urls_file:
            self.cached_urls_dict = json.load(cached_urls_file)
            cached_urls = set(self.cached_urls_dict['urls'])

        return cached_urls

    @property
    def cached_urls(self):
        """
        the list of cached urls from previous executions of the software.
        @return: the list of cached urls from previous executions of the software.
        @rtype: set
        """

        if not self._cached_urls:

            # caching for caching: I heard you like caching, so I implemented some caching inside your caching.
            self._cached_urls = self._get_cached_urls()
        return self._cached_urls

    def cache_new_urls(self):
        """
        takes all the new urls that were extracted, and stores them in the file for next usage.
        """
        self.cached_urls.update(self.urls_to_extract.difference(self.failed_urls))
        self.cached_urls_dict['urls'] = list(self.cached_urls)
        with io.open(PATH_TO_CACHED_URLS_FILE, 'w', encoding='utf-8') as cached_urls_file:
            json.dump(self.cached_urls_dict, cached_urls_file)

