from store_site import Detail
from cart.cart_extractor import CartExtractor


class CartData(Detail):
    """
    stores the data about whether there is a cart on site or not
    """

    @staticmethod
    def get_name():
        """
        @return: the name of the folder where the file exists
        @rtype: str
        """
        return "cart"

    def _get_extractor(self):
        """
        @return: the correct extractor to call to set this data
        @rtype: CartExtractor
        """
        return CartExtractor
