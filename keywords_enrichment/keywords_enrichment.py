import os
import io
import json
import urllib

GENERAL_KEYWORDS_FILE_NAME = "general_enrichment"
CUSTOMER_TYPE_KEYWORDS_FILE_NAME = "customer_type_enrichment"

ENRICHMENT_FILES = {"general": GENERAL_KEYWORDS_FILE_NAME,
                     "customer_type": CUSTOMER_TYPE_KEYWORDS_FILE_NAME}

class KeywordEnricher(object):

    @staticmethod
    def _get_enriched_keyword(keyword, file_name):
        path_to_keywords = os.path.join(os.path.abspath("keywords_enrichment"), '{}.json'.format(file_name))
        with io.open(path_to_keywords, encoding='utf8') as enrichment_file:
            keywords = json.loads(enrichment_file.read(), encoding='utf8')

        enriched = ""
        for key in iter(keywords.keys()):
            if key.upper() == keyword.upper():
                enriched = u"{}".format(" OR ".join(keywords[key]))

        return enriched

    @staticmethod
    def enrich_keyword(keyword, customer_type, keywords_to_exclude):
        general_enriched = "\"{}\" AND (".format(keyword) + KeywordEnricher._get_enriched_keyword(keyword, GENERAL_KEYWORDS_FILE_NAME)
        fully_enriched = "{}".format(general_enriched) + KeywordEnricher._get_enriched_keyword(customer_type, CUSTOMER_TYPE_KEYWORDS_FILE_NAME) + ")"
        if keywords_to_exclude:
            fully_enriched = "{} NOT {}".format(fully_enriched, " NOT ".join(keywords_to_exclude))


        return fully_enriched.encode('utf-8')

