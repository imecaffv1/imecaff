from bs4 import BeautifulSoup
import requests
from fake_useragent import UserAgent
import re
import json
import os
from urllib.parse import urlparse, urljoin
from typing import Pattern

"""
this utils file was created to move site parsing to one earlier stage and pass on all relevant parsed pages
to extractors, letting the choose which pages they need to extract their data.

can be refactored into class and smart keyword parsing in the future.
"""



PATH_TO_KEYWORDS = os.path.join(os.path.abspath("utils"), 'keywords.json')

with open(PATH_TO_KEYWORDS, 'r') as keywords_file:
    data = json.load(keywords_file)



def page_parsing(url):
    """
    gets a url, and with a fake user agent gets it and returns a bs4 parsed page.
    @param url: the url to get and parse
    @type url: str
    @return: the bs4 parsed url page
    @rtype: BeautifulSoup
    """
    ua = UserAgent()
    return BeautifulSoup(requests.get(url, headers={'User-Agent': ua.Chrome}, allow_redirects=False, timeout=(10, 6)).text, "lxml")



def get_regex_for_page_type(page_type):
    """
    returns concatenated regex according to page type
    @param page_type: the key in the data dictionary from keywords.json
    @type page_type: str
    @return: the concatenated regex
    @rtype: Pattern
    """
    return re.compile(r'|'.join(data.get(page_type)), flags=2)


# keep these here because it's not a class so functions need to be declared and implemented before usage.

ABOUT_REGEX = get_regex_for_page_type('about')
CONTACT_REGEX = get_regex_for_page_type('contact')
ADVERTISE_REGEX = get_regex_for_page_type('advertise')


PAGE_TYPES_REGEXES = {'about': ABOUT_REGEX,
                      'contact': CONTACT_REGEX,
                      'advertise': ADVERTISE_REGEX}


def get_parsed_pages(main_url):
    """
    extracting all the parsed relevant pages from a given url, if they exist on site according to regexes
    @param main_url: the url to extract the pages from
    @type main_url: str
    @return: dictionary where key is the type of page, and the value is the parsed page if was found, else None
    @rtype: dict
    """

    # parsing page from search engine, not necessarily the home web page of the site
    pages_per_type = {}
    parsed_main_page = page_parsing(main_url)
    pages_per_type['main_page'] = [parsed_main_page]
    links = parsed_main_page.find_all('a')

    # looking for rest of pages
    for page_type, type_regex in iter(PAGE_TYPES_REGEXES.items()):
        pages_per_type[page_type] = get_parsed_pages_for_type(main_url, links, page_type)
    return pages_per_type

def get_parsed_pages_for_type(main_url, links, page_type):
    """
    extracts all parsed pages from a site according to a type
    @param main_url: the url of the website as returned from the search engine
    @type main_url: str
    @param links: the links found on the page, received as argument to remove redundant call to find_all('a')
    @type links: list
    @param page_type: the type of page, should match a type from the keyword file
    @type page_type: str
    @return: a list of all parsed pages found for that type
    @rtype: list
    """
    pages = []
    for link in links:
        if PAGE_TYPES_REGEXES[page_type].search(link.get_text()):

            # had issues creating a generic function for full paths and relative paths, might need more tuning in future
            contact_url = urljoin(main_url, link.get('href'))
            if urlparse(contact_url).netloc:
                pages.append(page_parsing(contact_url))
    return pages
