from abc import ABCMeta, abstractmethod, abstractproperty
import re
import json
import os
from typing import Pattern

class IExtractor(object):
    __metaclass__ = ABCMeta

    def __init__(self, dir_name):
        self._version = None
        self._dir_name = dir_name

    @abstractmethod
    def extract_from_page(self, url):
        pass

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    @property
    def dir_name(self):
        return self._dir_name

    @dir_name.setter
    def dir_name(self, value):
        self._dir_name = value


class FromPageExtractor(IExtractor):

    @abstractmethod
    def extract_from_page(self, url, parsed_site):
        pass

    def __init__(self, dir_name):
        """
        gets the name of the directory where the extractor file is, in order to get the regex from it's keyword file,
        and calls func to generate the regex.
        @param dir_name: the directory where the extractor file is
        @type dir_name: str
        """
        super(FromPageExtractor, self).__init__(dir_name)
        self.regex = self.get_regex_from_keywords_file(dir_name)

    def get_regex_from_keywords_file(self, dir_name):
        """
        gets the name of the directory where the extractor file is, in order to get the regex from it's keyword file,
        and returns a concatenated regex with 'or' between the keywords
        @param dir_name: the directory where the extractor file is
        @type dir_name: str
        @return: concatenated regex with 'or' between the keywords
        @rtype: Pattern
        """
        path_to_keywords = os.path.join(os.path.abspath(dir_name), 'keywords.json')
        with open(path_to_keywords, 'r') as keywords_file:
            data = json.load(keywords_file)
            return re.compile(r'|'.join(data['keywords']), flags=2)