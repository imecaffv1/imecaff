from search_engines.bing_search_engine import BingSearchEngine


class Consts:
    """
    class used to hold the mapping between search engine names and the classes.
    add here when you want to enable the option to run the search from a different engine.
    """
    search_engines = {"bing": BingSearchEngine}