from search_engines import search_engine
import json
import requests
import store_site
import os


class BingSearchEngine(search_engine.BaseSearchEngine):

    PATH_TO_CONFIG_FILE = os.path.join(os.path.abspath("search_engines"), 'bing_config.json')
    

    def __init__(self):
        super(BingSearchEngine, self).__init__()
        self._name = "Bing"
        self._key = None
        self._config_id = None
        self.set_params_from_config()


    def set_params_from_config(self, path_to_config=PATH_TO_CONFIG_FILE):
        """
        reads config file for bing search engine and sets the attributes accordingly
        @param path_to_config: the path to the bing config file
        @type path_to_config: str
        """
        config_data = None
        with open(path_to_config) as json_file:
            config_data = json.load(json_file)

        self.config_id = config_data.get('custom_configuration_id')
        self.key = config_data.get('key_1')
        self._endpoint_url = config_data.get('url')
        # self.results_limit = config_data.get('number_of_results')


    def find_sites_and_ranking(self, search_keywords, market, number_of_results):

        self.results_limit = number_of_results
        stores = []
        num_of_searches = 1
        results_per_page = self.results_limit
        if self.results_limit > 50:
            num_of_searches = self.results_limit//50
            results_per_page = 50

        for search_number in range(num_of_searches):

            params = {"q": search_keywords, "customconfig": self.config_id, "format": "JSON", "safesearch": "Off",
                      "count": results_per_page, "offset": search_number*50, "mkt": market, "setLang": market[:2].upper()}
            response = requests.get(self.endpoint_url, headers={'Ocp-Apim-Subscription-Key': self.key}, params=params)

            data_from_response = response.json()

            if not response.status_code == 200:
                raise ValueError("getting list of sites from bing api didnt work, {}".format(response.text))
            else:
                for i, search_result in enumerate(data_from_response[u'webPages'][u'value']):
                    domain = store_site.Domain(search_result['url'], self.name, i+1)
                    stores.append(domain)

        return stores

    @property
    def config_id(self):
        """
        custom config id required as part of the query to bing, should be linked to an endpoint
        @return: custom config id required as part of the query to bing, should be linked to an endpoint
        @rtype: str
        """
        return self._config_id

    @config_id.setter
    def config_id(self, value):
        """
        custom config id required as part of the query to bing, should be linked to an endpoint
        @param value: custom config id required as part of the query to bing, should be linked to an endpoint
        @type value: str
        """
        self._config_id = value

    @property
    def results_limit(self):
        """
        the results limit that would be applied to the bing search
        @return: the results limit that would be applied to the bing search
        @rtype: int
        """
        return self._results_limit

    @results_limit.setter
    def results_limit(self, value):
        """
        the results limit that would be applied to the bing search
        @param value: the results limit that would be applied to the bing search, can be only between 10-50 including.
        raises Exception if invalid
        @type value: int
        """
        if not isinstance(value, int):
            raise TypeError("the results limit was set to {}, bing can only get an integer")
        elif not(value >= 1 and value <=500):
            raise ValueError("the results limit was set to {}, bing can only get an integer between 1-500")

        self._results_limit = value


    @property
    def key(self):
        """
        the subscription key used in the API call
        @return: the subscription key used in the API call
        @rtype: str
        """
        return self._key

    @key.setter
    def key(self, value):
        """
        @param value: the subscription key used in the API call
        @type value: str
        """
        self._key = value
