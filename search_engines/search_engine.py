from abc import ABCMeta, abstractmethod, abstractproperty

class SearchEngine(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def find_sites_and_ranking(self, search_keywords, market, number_of_results):
        """
        gets one keyword or more, and returns a list of urls and their ranking according to the search engine
        @param search_keywords: the keyword to search for
        @type search_keywords: str
        @param market: which market should the search engine search for, according to country codes format (en-US)
        @type market: str
        @return: the list of domain objects created for each of the urls from the search engines
        @rtype: list
        """

    @abstractmethod
    def set_params_from_config(self, path_to_config):
        """
        gets the path to the search engine config, and sets the parameters of the search engine according to it.

        @param path_to_config: the path to the config file, should be an absolute path to work on every computer
        @type path_to_config: str
        """


class BaseSearchEngine(SearchEngine):
    """
    class that implements functions and properties which will be relevant for several search engines.
    """

    def __init__(self):
        self._name = None
        self._results_limit = None
        self._endpoint_url = None
        self._search_term = None
        self._path_to_config = None


    @property
    def search_terms(self):
        """
        the search term to use when searching
        @return: the search term to use when searching
        @rtype: str
        """
        return self._search_term

    @search_terms.setter
    def search_terms(self, value):
        """
        the search term to use when searching
        @param value: the search term to use when searching
        @type value: str
        """
        self._search_term = value

    @property
    def name(self):
        """
        the name of the search engine, for mapping
        @return: the name of the search engine, for mapping
        @rtype: str
        """
        return self._name

    @name.setter
    @abstractmethod
    def name(self, value):
        """
        should be initialized within class implementation and not changed,
        no need to implement here, and no need to implement if you just want to set the private by yourself
        @param value: the name of the search engine as exists in the mapping dict
        @type value: str
        """
        pass

    @property
    def results_limit(self):
        """
        the limit of results to use when searching
        @return: the limit of results to use when searching
        @rtype: str
        """
        return self._results_limit

    @results_limit.setter
    @abstractmethod
    def results_limit(self, value):
        """
        should be implemented for every search engine, since the limits are different.
        """
        pass

    @property
    def endpoint_url(self):
        """
        the url to send the request to in order to get the search results
        @return: the url to send the request to in order to get the search results
        @rtype: str
        """
        return self._endpoint_url

    @endpoint_url.setter
    def endpoint_url(self, value):
        """
        the url to send the request to in order to get the search results
        @param value: the url to send the request to in order to get the search results
        @type value: str
        """
        self._endpoint_url = value
